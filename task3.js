function multiplyTripleArg(param1) {
    return function(param2) {
        return function(param3) {
        return param1 * param2*param3;
    }
    }    
}
console.log("TRIPLE ARG:"+multiplyTripleArg(3)(3)(3))
///////////////////////////////

var multiply = (function () {

   var sum=1;
   return    function (arg) {
       if(arg)
       {sum*=arg;
           return multiply;
       }
    //    else { var temp=sum;
    //        sum=1;
    else {
       return sum;
       }
   };

})();

 var res = multiply(3)(3)(3)(4)();
console.log("N ARG:"+res);
///////////////////////////////////
var generateMultiplyChain= (function(count){
    var res=1;
    function multiply(arg){
        if(--count)
    {  
            console.log("count "+count)
            res*=arg;
            console.log("RES: "+res);
            return multiply;
    } 
            else return res*arg;
    }
    return multiply;
})
var s = generateMultiplyChain(3);
console.log("MULTI ARG multiply:"+s(2)(3)(2));

//////////////////////////////////
function sum(arg1, arg2) {
    
    return arg1+arg2;
}
function multi(arg1, arg2) {
    return arg1*arg2;
}
var generateChain = (function(count,func){
    
    function operation(arg1){
    {  
            count--;
            return function oper(arg2){
                console.log("/count/ "+count)
                if(--count)
               { arg1=func(arg1,arg2);
                 console.log("RES:"+arg1)
                return oper;
                }
                else {
                    return func(arg1,arg2)
                };
                
            };
    } 
        
    }
    return operation;
})
 
 var a=generateChain(4,multi);
 console.log("MULTI ARG MULTI FUNC"+a(1)(2)(3)(4));